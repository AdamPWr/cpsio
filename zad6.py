#%matplotlib inline
#The line above is necesary to show Matplotlib's plots inside a Jupyter Notebook

import cv2
import numpy as np
# from matplotlib import pyplot as plt

# zad 1
c=0.5

# zad 2
m=0.45
e=8
contrastize = lambda r: (1/(1+((m/r)**e)))

# zad 3
gamma = 2


image = cv2.imread("serce.jpg")


cv2.imshow("Zdjecie", image)
cv2.waitKey(0)

img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("Szare zdjecie", img_gray)

img_gray2 = np.multiply(img_gray,c).astype(np.uint8)
cv2.waitKey(0)
cv2.imshow("Szare zdjecie c=0.5", img_gray2)

contrastize(np.array(img_gray))

cv2.waitKey(0)
cv2.imshow("Skontrastowane", img_gray)


img_gray3 = np.multiply(img_gray,c).astype(np.uint8)
cv2.waitKey(0)
cv2.imshow("Szare zdjecie c=0.5, gamma=2", img_gray3)
cv2.waitKey(0)
