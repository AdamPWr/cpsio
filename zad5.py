#%matplotlib inline
#The line above is necesary to show Matplotlib's plots inside a Jupyter Notebook

import cv2
import numpy as np
# from matplotlib import pyplot as plt

  

image = cv2.imread("rtg.png")
x1,x2,y1,y2,counter = 0,0,0,0,0



def on_click(event, x, y, p1, p2):
    global counter, x1, y1, x2, y2, image
    if event == cv2.EVENT_LBUTTONDOWN:
        counter+=1
        if counter == 1:
            x1,y1 = x,y
        if counter == 2:
            x2,y2 = x,y
            ROI = image[y1:y2, x1:x2]
            image = ROI
            cv2.imwrite("output.png", ROI)
            x1,x2,y1,y2,counter = 0,0,0,0,0


while(1):
    cv2.imshow("image", image)
    cv2.namedWindow('image')
    cv2.setMouseCallback('image', on_click)
    
    k = cv2.waitKey(20) & 0xFF
    if k == 27:
        break

# cv2.imwrite("output.png", ROI) 
